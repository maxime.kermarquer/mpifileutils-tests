#!/bin/bash
#SBATCH --partition=copy
#SBATCH --ntasks=56
#SBATCH --nodes=2
#SBATCH --mem=250G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
##SBATCH --output=outputs/test-dwalk-2-nodes-%j.txt
#SBATCH --output=/tmp/test-dwalk-2-nodes-%j.txt # lustre 1 read only
##SBATCH --error=outputs/test-dwalk-2-nodes-%j.txt
#SBATCH --error=/tmp/test-dwalk-2-nodes-%j.txt # lustre 1 read only
#SBATCH --job-name=mpifileutils-test-dwalk

sinfo -p copy

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load mpifileutils

echo -e "--- \e[1;34mStart\e[0m ---"

mpirun -np ${SLURM_NTASKS} dwalk -v /network/lustre/dtlake01/dsi/maxime.kermarquer

echo -e "--- \e[1;34mEnd\e[0m ---"
