#!/bin/bash
#SBATCH --partition=copy
#SBATCH --ntasks=28
#SBATCH --mem=250G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
##SBATCH --output=outputs/test-dwalk-%j.txt # lustre 1 read only
#SBATCH --output=/tmp/test-dwalk-%j.txt
##SBATCH --error=outputs/test-dwalk-%j.txt # lustre 1 read only
#SBATCH --error=/tmp/test-dwalk-%j.txt
#SBATCH --job-name=mpifileutils-test-dwalk

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load mpifileutils

echo -e "--- \e[1;34mStart\e[0m ---"

mpirun -np ${SLURM_NTASKS} dwalk -v /network/lustre/dtlake01/dsi/maxime.kermarquer

echo -e "--- \e[1;34mEnd\e[0m ---"
